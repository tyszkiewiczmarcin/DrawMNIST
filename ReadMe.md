## Program that recognises drawn digits using different algorithms  


![](showcase.gif)  

GUI libraries:  
 * pygame  
 * pygame-gui

Algorithms libraries:  
 * scikit-learn
 * tensorflow & keras