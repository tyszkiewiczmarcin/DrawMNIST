import pygame
import pygame_gui

import numpy as np
from itertools import product

import pickle
import keras


color = {
    'background': '#474747',
    'grid': '#A8A7A7',
    'prediction': '#F26B38'
}

class Image(object):
    def __init__(self, window, image_res_unit, pixel_size, brush_transparency):
        self.window = window
        
        self.image_res_unit = image_res_unit
        self.pixel_size = pixel_size
        self.brush_transparency = brush_transparency
        
        self.content = np.zeros(shape=(self.image_res_unit, self.image_res_unit), dtype=float)
        
        self.knn_model = pickle.load(open('models/knn_clf.sav', 'rb'))
        self.dl_model = keras.models.load_model('models/dl_clf.h5')
       
       
    def clear(self):
        self.content = np.zeros(shape=(self.image_res_unit, self.image_res_unit), dtype=float)
        
    def paint(self, x, y):
        """Progressively fill in nearby pixels."""
        def softness(dx, dy):
            dist4 = (dx**2 + dy**2)**2
            return 1.0 / (1.0 + dist4) * self.brush_transparency
        
        brush_area = [(dx, dy) for dx, dy in product(range(-2,3), range(-2,3))]
        
        x, y = x // self.pixel_size, y // self.pixel_size
        
        for dx, dy in brush_area:
            xx = x + dx
            yy = y + dy
            if 0 < xx > self.image_res_unit - 1 or 0 < yy > self.image_res_unit - 1:
                continue
            
            self.content[xx, yy] += softness(dx, dy)
            self.content[xx, yy] = min(1.0, self.content[xx, yy])
            
        
    def display(self):
        for x, row in enumerate(self.content):
            for y, value in enumerate(row):
                rect = pygame.Rect(
                    x * self.pixel_size, 
                    y * self.pixel_size, 
                    self.pixel_size, 
                    self.pixel_size)
    
                grayscale = 255 * (1 - value)
                pygame.draw.rect(self.window, (grayscale, grayscale, grayscale), rect, 0)
                pygame.draw.rect(self.window, color['grid'], rect, 1)   # Draw grid
     
    
    def predict(self, method):
        image = np.swapaxes(self.content, 0, 1) * 255
        flatten = image.reshape(1, -1)
        
        if method == 'KNN':
            prediction = self.knn_model.predict(flatten)[0]
        elif method == 'NN':
            prediction = np.argmax(self.dl_model.predict(flatten))
        else:
            raise 'ERROR :: Unrecognised model name'
        
        return prediction


class Prog(object):
    def __init__(self, pixel_size, image_size, brush_transparency):
        self.pixel_size = pixel_size
        self.image_res_unit = int(np.sqrt(image_size))
        self.image_res = self.image_res_unit * self.pixel_size
        self.gui_width = self.image_res // 4
        self.window_height = self.image_res
        self.window_width = self.image_res + self.gui_width
        self.button_dim = (5 * self.pixel_size, 3 * self.pixel_size)
        self.button_clear_pos = (self.image_res + self.pixel_size, 2 * self.pixel_size)
        self.button_run_knn_pos = (self.image_res + self.pixel_size, 8 * self.pixel_size)
        self.button_run_dl_pos = (self.image_res + self.pixel_size, 14 * self.pixel_size)
        self.textbox_output_info_pos = (self.image_res + self.pixel_size, 19.5 * self.pixel_size)
        self.textbox_output_digit_pos = (self.image_res + self.gui_width // 2 - 32, 20 * self.pixel_size)
        
        pygame.init()
        pygame.display.set_caption('MNIST App')
        self.window = pygame.display.set_mode((self.window_width, self.window_height))
        
        self.background = pygame.Surface((self.window_width, self.window_height))
        self.background.fill(pygame.Color(color['background']))
        
        self.manager = pygame_gui.UIManager((self.window_width, self.window_height))
        
        self.button_clear = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect(self.button_clear_pos, self.button_dim),
            text='CLEAR', 
            manager=self.manager)
        
        self.button_run_knn = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect(self.button_run_knn_pos, self.button_dim),
            text='RUN KNNeighbors', 
            manager=self.manager)
        
        self.button_run_dl = pygame_gui.elements.UIButton(
            relative_rect=pygame.Rect(self.button_run_dl_pos, self.button_dim),
            text='RUN Neaural Network', 
            manager=self.manager)
        
        self.font_big_obj = pygame.font.Font('fonts/JerseyM54.ttf', 128)
        self.font_small_obj = pygame.font.SysFont('futura', 15)
        
        self.image = Image(
            window=self.window,
            image_res_unit=self.image_res_unit,
            pixel_size=self.pixel_size,
            brush_transparency=brush_transparency
        )
        
        
    def run(self):
        clock = pygame.time.Clock()
        pred_dig_surface = None
        textbox_info_surface = None
        is_running = True
        
        while is_running:
            time_delta = clock.tick(60) / 1000.0
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT or (event.type == pygame.KEYUP and event.key == pygame.K_ESCAPE):
                    is_running = False

                if event.type == pygame.USEREVENT:
                    if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                        if event.ui_element == self.button_clear:
                            self.image.clear()
                            pred_dig_surface = None
                            textbox_info_surface = None
                            
                        if event.ui_element == self.button_run_knn:
                            prediction = self.image.predict(method='KNN')
                            pred_dig_surface = self.font_big_obj.render(str(prediction), False, color['prediction'])
                            textbox_info_surface = self.font_small_obj.render('Predicted using KNN:', False, color['prediction'])
                            
                        if event.ui_element == self.button_run_dl:
                            prediction = self.image.predict(method='NN')
                            pred_dig_surface = self.font_big_obj.render(str(prediction), False, color['prediction'])
                            textbox_info_surface = self.font_small_obj.render('Predicted using NN:', False, color['prediction'])
                            
                self.manager.process_events(event)
                
            if pygame.mouse.get_pressed()[0]:
                x, y = pygame.mouse.get_pos()
                if x > 0:
                    self.image.paint(x, y)
                
            self.update_surfaces(time_delta, pred_dig_surface, textbox_info_surface)
              
                
    def update_surfaces(self, time_delta, pred_dig_surface, textbox_info_surface):
        self.manager.update(time_delta)
        
        self.window.blit(self.background, (0, 0))
        
        self.manager.draw_ui(self.window)
        self.image.display()
        
        if textbox_info_surface:
            self.window.blit(textbox_info_surface, self.textbox_output_info_pos)
        
        if pred_dig_surface:
            self.window.blit(pred_dig_surface, self.textbox_output_digit_pos)
            
        pygame.display.update()
            
            
def main():
    prog = Prog(
        pixel_size=30,
        image_size=784,
        brush_transparency=0.25
    )
    prog.run()
    
                
if __name__ == "__main__":
    main()
